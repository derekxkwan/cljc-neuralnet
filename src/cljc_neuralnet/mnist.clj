
(ns cljc-neuralnet.mnist
  (:require [clojure.java.io :as io]
            [clojure.core.matrix :as m :include-macros true]
            [clojure.string :as str]))

;; uses mnist in csv format as found on kaggle or other fine repositories
;;csv has header label,1x1,1x2,...1x28,2x1,2x2,....,28x28
;;values range from 0-255 (except labels which are 0-9)
(def files {:train "mnist_train.csv" :test "mnist_test.csv"})

(def train-data (atom nil))
(def test-data (atom nil))


(defn translate-label [ipt]
  ;; translate numerical label to indicator vec
  (let [my-zerovec (vec (repeat 10 0))]
    (assoc my-zerovec (read-string ipt) 1))
  )

(defn read-file [file-type]
  (let [filename (str "/home/dxk/datasets/mnist/" (get files file-type))
        x-labels (atom [])
        x-data (atom [])]
    (with-open [rdr (io/reader filename)]
      (doseq [cur-line (line-seq rdr)]
        (when (nil? (re-matches #"label,.*" cur-line))
          (let [splitted (str/split cur-line #"\s*,\s*")
                cur-label (first splitted)
                cur-data (vec (map #(read-string %) (rest splitted)))]
            ;; note for cljs, use (map #(js/parseInt)...)
            (swap! x-labels conj cur-label)
            (swap! x-data conj cur-data)
            ))
        ))
    {:labels (m/matrix (map translate-label @x-labels))
     :data (m/matrix @x-data)}
    ))

(defn get-train-data [] @train-data)
(defn get-test-data [] @test-data)

(defn load-mnist []
  (when (nil? @train-data) (reset! train-data (read-file :train)))
  (when (nil? @test-data) (reset! test-data (read-file :test)))
  )
      
      
