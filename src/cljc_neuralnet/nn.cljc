(ns cljc-neuralnet.nn
  (:require [clojure.core.matrix :as m :include-macros true]
            [clojure.core.matrix.selection :as msel]
            [clojure.core.matrix.stats :as mstats]
            ;;[thinktopic.aljabr.core :as nd :include-macros true]
            ))

;; use vectorz for now until i figure out aljabr dependency
(m/set-current-implementation :vectorz)

(defn sigmoid [z]
  ((comp (partial m/div 1.0) (partial m/add 1.0) m/exp m/negate) z))

(defn sigmoid-deriv [z]
  (let [sig-z (sigmoid z)]
    (m/mul sig-z (m/sub 1.0 sig-z)))
  )

(defn cost-deriv [output-activations labels]
  (m/sub output-activations labels))


;; translate a result matrix of shape [num-examples features] to digits
(defn translate-indicators [res]
  (map (comp first (partial apply max-key second) (partial map-indexed vector) m/eseq) (m/rows res))
  )

(defprotocol NNLearn
  "learning methods for neural networks"
  (backprop [self ex-data ex-labels] "backpropagation on a set of training examples")
  (feedforward [self ipt] "return output of the network")
  (num-correct [self test-data] "return num correct of test-data")
  (grad-desc-single [self batch learn-rate] "single step of gradient descent")
  (sto-grad-desc [self train-ex num-epochs batch-size learn-rate test-ex]
    "stochastic gradient descent")
  (train [ipt-atom train-data num-epochs batch-size learn-rate test-data]
    "train neural network (with side effects...)")
  )
    

(defrecord NNetwork [num-layers sizes weights biases]
  NNLearn
  (backprop [self ex-data ex-labels]
    (let [cur-state (atom self)
          cur-weights (get @cur-state :weights)
          cur-biases (get @cur-state :biases)
          partial-weights (atom (mapv #(m/zero-array (m/shape %)) cur-weights))
          partial-biases (atom (mapv #(m/zero-array (m/shape %)) cur-biases))
          activations (atom [ex-data])
          zs (atom [])
          ]
      ;; biases shape [num-neurons-in-layer]
      ;; weights shape [num-neurons-in-layer num-inputs] ([num-neurons 784] for first non-ipt layer) 
      ;; ex-data is in the format num-examples x 784
      ;; so first dot is shape [num-examples number-of-neurons]
      ;; ex-labels is in the format [num-examples label-dim]
      ;; here we iterate through each layer
      ;;(println "init done")
      (loop [w-left cur-weights
             b-left cur-biases
             activation ex-data]
        ;;(println (m/shape (first w-left)))
        ;;(println (m/shape (first b-left)))
        ;;(println (m/shape activation))
        ;;(println "==========")
        (let [w (first w-left)
              b (first b-left)
              z (m/add (m/mmul activation (m/transpose w)) b)
              new-activation (sigmoid z)]
          (swap! zs conj z)
          (swap! activations conj new-activation)
          (when (and (> (count b-left) 1) (> (count w-left) 1))
            (recur (rest w-left) (rest b-left) new-activation))
          ))
      ;;(println "front-pass done")
      ;;backward pass
      ;; indices a little weirder so just use indexing
      ;; loop from last layer to second layer (firs iteration is error output)
      ;; want error (delta) for every neuron meaning shape [num-examples num-neurons-in-layer]
      ;; for mnist, shape of last layer of activations (and zs) is [num-examples num-outputs]

      (loop [i (dec (count cur-biases))
             cur-delta (m/mul (cost-deriv (last @activations) ex-labels)
                              (sigmoid-deriv (last @zs)))

             ]
        
        ;;(println i)
        (swap! partial-biases assoc i (mstats/sum cur-delta))
        ;; want shape [num-outputs num-neurons-last-hidden-layer]
        ;; second to last activations has shape [num-examples num-neurons-last layer]
        ;; want: [num-neurons-in-i num-neurons-in-i-min 1
        (swap! partial-weights assoc i
               (m/mmul (m/transpose cur-delta)
                       (get @activations i)
                       ))

        ;; cur-delta recur is pre-decrement (decrease indices by 1)
        ;; w shape [num-neurons-in-i num-neurons-in-i-min-1],delta shape [num-examples, num-neurons-in-i]

        (when (> i 0)
          (recur (dec i)
                 (m/mul (m/mmul cur-delta (get cur-weights i))
                         (sigmoid-deriv (get @zs (dec i))))

                 ))
        )
      {:partial-weights @partial-weights :partial-biases @partial-biases}
      ))
    
  (feedforward [self ipt]
    (let [biases (get self :biases)
          weights (get self :weights)
          new-coll (mapv vector weights biases)]
      ;; ipt shape [num-examples num-neurons], weights [num-neurons-next-layer, num-neurons-cur-layer]

      ;;error, mismatched size [10000, 30] vs [10,30]
      ;;(println (m/shape ipt))
      (reduce (fn [n [w b]] (m/add (m/mmul n (m/transpose w)) b)) ipt new-coll)
      ))
    
  (num-correct [self test-data]
    (let [cur-labels (get test-data :labels)
          cur-data (get test-data :data)
          res (.feedforward self cur-data)
          res-labels (translate-indicators res)
          test-labels (translate-indicators cur-labels)
          ]

      ((comp count (partial filter true?)) (map = res-labels test-labels))
      ))
                 
  (grad-desc-single [self batch learn-rate]
    (let [cur-state (atom self)
          cur-weights (get @cur-state :weights)
          cur-biases (get @cur-state :biases)
          partial-weights (atom (mapv #(m/zero-array (m/shape %)) cur-weights))
          partial-biases (atom (mapv #(m/zero-array (m/shape %)) cur-biases))
          res (.backprop self (get batch :data) (get batch :labels))
          delta-partial-w (get res :partial-weights)
          delta-partial-b (get res :partial-biases)]
      ;;(println "finish init")
      ;;(println (count delta-partial-w))
     ;; (doseq [x delta-partial-w] (println (m/shape x)))
      ;;(println (count @partial-weights))
      ;;(doseq [x @partial-weights] (println (m/shape x)))
      ;;(println "=========")
      ;;(println (count delta-partial-b))
      ;;(doseq [x delta-partial-b] (println (m/shape (mstats/sum x))))
      ;;(println (count @partial-biases))
      ;;(doseq [x @partial-biases] (println (m/shape x)))

      ;; 2 [30 784] [10 784] 2 [30 784] [10 30] // 2 [30] [10 10] 2 [30] [10]
      (swap! partial-weights #(mapv m/add % delta-partial-w))
      ;;(println "finish add weights")
      (swap! partial-biases #(mapv m/add % delta-partial-b))
      ;;(println "finish add biases")
      (let [batch-n (get (m/shape (get batch :labels)) 0)
            scaled-rate (/ learn-rate batch-n)
            update-weights (mapv #(m/sub %1 (m/mul scaled-rate %2)) cur-weights @partial-weights)
            update-biases (mapv #(m/sub %1 (m/mul scaled-rate %2)) cur-biases @partial-biases)
            ]
        ;;(println "finish calc")
        (swap! cur-state merge {:weights update-weights :biases update-biases})
        )
      )
    )

  (sto-grad-desc [self train-ex num-epochs batch-size learn-rate test-ex]
    (let [test-labels (get test-ex :labels)
          train-labels (get train-ex :labels)
          train-data (get train-ex :data)
          test-n (get (m/shape test-labels) 0)
          train-n (get (m/shape train-labels) 0)
          cur-state (atom self)
          ]
      (doseq [cur (range num-epochs)
           :let [test-range (range test-n)
                 cur-shuf (shuffle test-range)
                 part-shuf (partition batch-size batch-size nil cur-shuf)
                 ]]
        ;;(println cur)
        (doseq [batch-idxs part-shuf
                :let [batch-labels (msel/sel train-labels batch-idxs (msel/irange))
                      batch-data (msel/sel train-data batch-idxs (msel/irange))
                      batch {:labels batch-labels :data batch-data}                 
                      results (.grad-desc-single @cur-state batch learn-rate)]]
          (swap! cur-state merge results)
          )
        (if (> test-n 0)
          (printf "epoch %d: %d / %d\n" cur (.num-correct @cur-state test-ex) test-n)
          )
          (printf "epoch %d complete\n" cur))
        
        )
      
      @cur-state
      ))
  
  (train [ipt-atom train-data num-epochs batch-size learn-rate test-data]
      (swap! ipt-atom sto-grad-desc train-data num-epochs batch-size learn-rate test-data)
      )
    )

(defn nnetwork-new [sizes]
  ;; biases is a vec with shapes [num-neurons-in-layer] with each entry being a layer (excluding input layer)
  ;; weights is a vec with shapes [num-neurons-in-layer num-inputs-for-each-neuron] with each entry being a layer
  ;; (excluding input layer)
  ;; so in mnist case the first non-ipt layer has weight shape [num-neurons 784]
  ;; since 784 is our input size
  (let [num-layers (count sizes)
        biases (mapv
                #(m/array (repeatedly % (fn [& n] (rand 1))))
                (rest sizes))
        weights (mapv
                 #(m/reshape
                   (m/array (repeatedly (* %1 %2) (fn [& n] (rand 1)))) [%2 %1])
                 (drop-last sizes) (rest sizes))
        ]
         (NNetwork. num-layers sizes weights biases)
         )
  )
