(ns cljc-neuralnet.core
  (:require [cljc-neuralnet.nn :as nn]
            [cljc-neuralnet.mnist :as mnist]
            [clojure.core.matrix :as m :include-macros true]
            ))
            
(mnist/load-mnist)


(def mnist-nn (atom (nn/nnetwork-new [784 30 10])))

;;(nn/train mnist-nn (mnist/get-train-data) 30 10 3.0 (mnist/get-test-data))
;;(nn/sto-grad-desc @mnist-nn (mnist/get-train-data) 30 10 3.0 (mnist/get-test-data))
(swap! mnist-nn nn/sto-grad-desc (mnist/get-train-data) 30 10 3.0 (mnist/get-test-data))

(let [cur-test (mnist/get-test-data)
      test-n (get (m/shape (get cur-test :labels)) 0)
      cur-correct (nn/num-correct @mnist-nn (mnist/get-test-data))
      pct-correct (double (/ cur-correct test-n))]
  (printf "%f correct (%d out of %d)" pct-correct cur-correct test-n)
)
