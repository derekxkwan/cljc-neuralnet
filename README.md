# cljc-neuralnet

Building a toy neural network working through [Neural Networks and Deep Learning](http://neuralnetworksanddeeplearning.com) to (re)learn neural networks. CLJC for Clojure/Clojurescript cross-compat (but still working out the `aljabr` vector lib for cljs so using `vectorz-clj` for now...)

Work very under progress...
